// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRx from 'vue-rx'
import Vue2Filters from 'vue2-filters'

import Observable from 'rxjs/Observable'
import Subscription from 'rxjs/Subscription'
import Subject from 'rxjs/Subject'
import BehaviorSubject from 'rxjs/BehaviorSubject'
import ReplaySubject from 'rxjs/ReplaySubject'

import App from './app'
import router from './router/router'

import { Auth } from './services/auth'
import { Database } from './services/db'
import { CurrentBudget } from './services/current-budget'
import { SettingsDate } from './services/settings-date'
import { Theme } from './services/theme'

// import * as OfflinePluginRuntime from 'offline-plugin/runtime'

Vue.use(Vue2Filters)
Vue.use(VueRx, {
  Observable,
  Subscription,
  Subject,
  BehaviorSubject,
  ReplaySubject
})

Vue.config.productionTip = false

Vue.prototype.$auth = new Auth()
Vue.prototype.$db = new Database().$get()
Vue.prototype.$current = new CurrentBudget()
Vue.prototype.$theme = new Theme()
Vue.prototype.$date = new SettingsDate()

Vue.filter('currencyInt', function (value, zero = true, digits = 2) {
  if (typeof value === 'number' && !isNaN(value)) {
    if (!zero && value === 0) {
      return null
    }
    return (value / Math.pow(10, digits)).toFixed(digits)
  } else {
    return value
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})

// OfflinePluginRuntime.install()
