export function SettingsDate () {
  function getDateFormat () {
    return localStorage.getItem('dateFormat')
  }

  function setDateFormat (format) {
    localStorage.setItem('dateFormat', format)
  }

  function dateDisplay (date) {
    var currDay = date.getDate()
    var currMonth = date.getMonth() + 1
    var currYear = date.getFullYear()
    var currFormat = getDateFormat()

    if (currFormat === 'YY/MM/DD') {
      return String(currYear).slice(2, 4) + '/' + currMonth + '/' + currDay
    }

    if (currFormat === 'DD/MM/YY') {
      return currDay + '/' + currMonth + '/' + String(currYear).slice(2, 4)
    }

    if (currFormat === 'MM/DD/YY') {
      return currMonth + '/' + currDay + '/' + String(currYear).slice(2, 4)
    }
  }

  return {
    getDateFormat,
    setDateFormat,
    dateDisplay
  }
}
